import { Action } from '../actions';
import Restaurant from '../../types/Restaurant';
import { detailsReducer, restaurantReducer } from '../reducer';

const restaurant: Restaurant = {
  backgroundImageURL: 'https://my.web.site/image.png',
  category: 'Category',
  location: {
    address: '123 Street St.',
    cc: 'US',
    city: 'City',
    country: 'United States',
    formattedAddress: [],
    lat: 1,
    lng: 2,
    postalCode: 75022,
    state: 'TX',
  },
  name: 'Restaurant',
};

describe('Details reducer', () => {
  it('should reduce HIDE_DETAILS with no state', () => {
    const action: Action = {
      type: 'HIDE_DETAILS',
    };

    const expected: null = null;
    const actual: Restaurant | null = detailsReducer(undefined, action);

    expect(actual).toEqual(expected);
  });

  it('should reduce HIDE_DETAILS with previous state', () => {
    const action: Action = {
      type: 'HIDE_DETAILS',
    };

    const expected: null = null;
    const actual: Restaurant | null = detailsReducer(restaurant, action);

    expect(actual).toEqual(expected);
  });

  it('should reduce SHOW_DETAILS with no state', () => {
    const action: Action = {
      restaurant: restaurant,
      type: 'SHOW_DETAILS',
    };

    const expected: Restaurant = restaurant;
    const actual: Restaurant | null = detailsReducer(undefined, action);

    expect(actual).toEqual(expected);
  });

  it('should reduce SHOW_DETAILS with previous state', () => {
    const previousState: Restaurant = {
      ...restaurant,
      name: 'Previous state',
    };
    const action: Action = {
      restaurant: restaurant,
      type: 'SHOW_DETAILS',
    };

    const expected: Restaurant = restaurant;
    const actual: Restaurant | null = detailsReducer(previousState, action);

    expect(actual).toEqual(expected);
  });

  it('should not reduce GET_RESTAURANTS', () => {
    const action: Action = {
      type: 'GET_RESTAURANTS',
    };

    const expected: null = null;
    const actual: Restaurant | null = detailsReducer(undefined, action);

    expect(actual).toEqual(expected);
  });

  it('should not reduce SET_RESTAURANTS', () => {
    const action: Action = {
      restaurants: [restaurant],
      type: 'SET_RESTAURANTS',
    };

    const expected: null = null;
    const actual: Restaurant | null = detailsReducer(undefined, action);

    expect(actual).toEqual(expected);
  });
});

describe('Restaurant reducer', () => {
  it('should reduce SET_RESTAURANTS with no state', () => {
    const restaurants: Restaurant[] = [restaurant];
    const action: Action = {
      restaurants: restaurants,
      type: 'SET_RESTAURANTS',
    };

    const expected: Restaurant[] = restaurants;
    const actual: Restaurant[] = restaurantReducer(undefined, action);

    expect(actual).toEqual(expected);
  });

  it('should reduce SET_RESTAURANTS with previous state', () => {
    const previousState: Restaurant[] = [
      {
        ...restaurant,
        name: 'Previous state',
      },
    ];
    const restaurants: Restaurant[] = [restaurant];
    const action: Action = {
      restaurants: restaurants,
      type: 'SET_RESTAURANTS',
    };

    const expected: Restaurant[] = restaurants;
    const actual: Restaurant[] = restaurantReducer(previousState, action);

    expect(actual).toEqual(expected);
  });

  it('should not reduce GET_RESTAURANTS', () => {
    const action: Action = {
      type: 'GET_RESTAURANTS',
    };

    const expected: Restaurant[] = [];
    const actual: Restaurant[] = restaurantReducer(undefined, action);

    expect(actual).toEqual(expected);
  });

  it('should not reduce HIDE_DETAILS', () => {
    const action: Action = {
      type: 'HIDE_DETAILS',
    };

    const expected: Restaurant[] = [];
    const actual: Restaurant[] = restaurantReducer(undefined, action);

    expect(actual).toEqual(expected);
  });

  it('should not reduce SHOW_DETAILS', () => {
    const action: Action = {
      restaurant: restaurant,
      type: 'SHOW_DETAILS',
    };

    const expected: Restaurant[] = [];
    const actual: Restaurant[] = restaurantReducer(undefined, action);

    expect(actual).toEqual(expected);
  });
});
