import { call, put } from 'redux-saga/effects';

import { fetchRestaurants, getRestaurants } from '../saga';
import { setRestaurants } from '../actions';
import Restaurant from '../../types/Restaurant';

describe('Get restaurants saga', () => {
  const restaurants: Restaurant[] = [
    {
      backgroundImageURL: 'https://my.web.site/image.png',
      category: 'Category',
      location: {
        address: '123 Street St.',
        cc: 'US',
        city: 'City',
        country: 'United States',
        formattedAddress: [],
        lat: 1,
        lng: 2,
        postalCode: 75022,
        state: 'TX',
      },
      name: 'Restaurant',
    },
  ];

  it('should take GET_RESTAURANTS', () => {
    const gen = getRestaurants();

    expect(gen.next().value).toEqual(call(fetchRestaurants));
    expect(gen.next(restaurants).value).toEqual(
      put(setRestaurants(restaurants))
    );
    expect(gen.next().done).toBeTruthy();
  });
});
