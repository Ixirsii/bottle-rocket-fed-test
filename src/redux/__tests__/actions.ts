import {
  Action,
  getRestaurants,
  hideDetails,
  setRestaurants,
  showDetails,
} from '../actions';
import Restaurant from '../../types/Restaurant';

describe('Redux action creators', () => {
  const restaurant: Restaurant = {
    backgroundImageURL: 'https://my.web.site/image.png',
    category: 'Category',
    location: {
      address: '123 Street St.',
      cc: 'US',
      city: 'City',
      country: 'United States',
      formattedAddress: [],
      lat: 1,
      lng: 2,
      postalCode: 75022,
      state: 'TX',
    },
    name: 'Restaurant',
  };

  it('should create a GetRestaurants action', () => {
    const expected: Action = {
      type: 'GET_RESTAURANTS',
    };

    const actual: Action = getRestaurants();

    expect(actual).toEqual(expected);
  });

  it('should create a HideDetails action', () => {
    const expected: Action = {
      type: 'HIDE_DETAILS',
    };

    const actual: Action = hideDetails();

    expect(actual).toEqual(expected);
  });

  it('should create a SetRestaurants action', () => {
    const restaurants = [restaurant];
    const expected: Action = {
      restaurants: restaurants,
      type: 'SET_RESTAURANTS',
    };

    const actual: Action = setRestaurants(restaurants);

    expect(actual).toEqual(expected);
  });

  it('should create a ShowDetals action', () => {
    const expected: Action = {
      restaurant: restaurant,
      type: 'SHOW_DETAILS',
    };

    const actual: Action = showDetails(restaurant);

    expect(actual).toEqual(expected);
  });
});
