import { combineReducers } from 'redux';

import Restaurant from '../types/Restaurant';

import { Action } from './actions';

/**
 * Reducer for detail modal state.
 *
 * @param state Previous state.
 * @param action Dispatched action.
 */
export const detailsReducer = (
  state: Restaurant | null = null,
  action: Action
): Restaurant | null => {
  switch (action.type) {
    case 'HIDE_DETAILS':
      return null;
    case 'SHOW_DETAILS':
      return action.restaurant;
    default:
      return state;
  }
};

/**
 * Reducer for restaurants state.
 *
 * @param state Previous state.
 * @param action Dispatched action.
 */
export const restaurantReducer = (
  state: Restaurant[] = [],
  action: Action
): Restaurant[] => {
  switch (action.type) {
    case 'SET_RESTAURANTS':
      return action.restaurants;
    default:
      return state;
  }
};

/**
 * Combine reducers.
 */
export default combineReducers({
  details: detailsReducer,
  restaurants: restaurantReducer,
});
