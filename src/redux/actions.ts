import Restaurant from '../types/Restaurant';

/**
 * Action creator to fetch restaurants list.
 */
export const getRestaurants = () => ({
  type: 'GET_RESTAURANTS' as const,
});

/**
 * Action creator to show details for a restaurant.
 */
export const hideDetails = () => ({
  type: 'HIDE_DETAILS' as const,
});

/**
 * Action creator to set restaurants state.
 *
 * @param {Restaurant[]} restaurants List of restaurants to set.
 */
export const setRestaurants = (restaurants: Restaurant[]) => ({
  restaurants: restaurants,
  type: 'SET_RESTAURANTS' as const,
});

/**
 * Action creator to close the details modal.
 *
 * @param {Restaurant} restaurant Restaurant to show details for.
 */
export const showDetails = (restaurant: Restaurant) => ({
  restaurant: restaurant,
  type: 'SHOW_DETAILS' as const,
});

/**
 * Action type with type guards.
 */
export type Action =
  | ReturnType<typeof getRestaurants>
  | ReturnType<typeof hideDetails>
  | ReturnType<typeof setRestaurants>
  | ReturnType<typeof showDetails>;
