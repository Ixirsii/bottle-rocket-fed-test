import { all, call, put, StrictEffect, takeLatest } from 'redux-saga/effects';

import Restaurant from '../types/Restaurant';

import { setRestaurants } from './actions';

const ENDPOINT: string =
  'https://s3.amazonaws.com/br-codingexams/restaurants.json';

/**
 * Fetch list of restaurants from S3 endpoint.
 */
export const fetchRestaurants = (): Promise<Restaurant[]> => {
  return fetch(ENDPOINT)
    .then((response) => response.json())
    .then((data) => (data as any).restaurants);
};

/**
 * Call fetchRestaurants API then dispatch an action to update state.
 */
export function* getRestaurants(): Generator<StrictEffect, any, any> {
  const restaurants: Restaurant[] = yield call(fetchRestaurants);

  yield put(setRestaurants(restaurants));
}

/**
 * Combine sagas.
 */
export default function* saga(): Generator {
  yield all([takeLatest('GET_RESTAURANTS', getRestaurants)]);
}
