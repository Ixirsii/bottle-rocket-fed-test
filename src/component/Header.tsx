import React, { FC, ReactElement } from 'react';

import mapIcon from '../images/icon_map@2x.png';

/**
 * Page header component.
 *
 * @constructor
 */
const Header: FC = (): ReactElement => (
  <div className='header'>
    <h1>
      Lunch Tyme <img alt='map' src={mapIcon} />
    </h1>
  </div>
);

export default Header;
