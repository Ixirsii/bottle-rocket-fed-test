import React from 'react';
import { render, screen } from '@testing-library/react';

import Header from '../Header';

describe('Header Component', () => {
  test('renders learn react link', () => {
    render(<Header />);

    const linkElement = screen.getByText(/Lunch Tyme/i);

    expect(linkElement).toBeInTheDocument();
  });
});
