import React, { FC, ReactElement, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import '../css/App.css';
import { Action, getRestaurants } from '../redux/actions';
import Restaurant from '../types/Restaurant';
import State from '../types/State';

import Header from './Header';
import Details from './details';
import RestaurantList from './list';

/**
 * Wrapped calls to action creators which automatically dispatch the action.
 */
interface DispatchProps {
  /** Dispatch wrapper for getRestaurants. */
  readonly dispatchGetRestaurants: () => Action;
}

/**
 * State props which are wired in from Redux.
 */
interface StateProps {
  /** List of restaurants from Redux state. */
  readonly restaurants: Restaurant[];
}

/**
 * Combined prop type.
 */
type Props = DispatchProps & StateProps;

/**
 * Main app component.
 *
 * @constructor
 */
const App: FC<Props> = ({
  dispatchGetRestaurants,
  restaurants,
}: Props): ReactElement => {
  useEffect(() => {
    dispatchGetRestaurants();
  }, [dispatchGetRestaurants]);

  return (
    <>
      <Header />
      <RestaurantList restaurants={restaurants} />
      <Details />
    </>
  );
};

/**
 * Connect {@code DispatchProps} to Redux.
 */
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps =>
  bindActionCreators(
    {
      dispatchGetRestaurants: getRestaurants,
    },
    dispatch
  );

/**
 * Connect {@code StateProps} to Redux.
 */
const mapStateToProps = (state: State): StateProps => ({
  restaurants: state.restaurants,
});

/*
 * Connect App component to Redux.
 */
export default connect<StateProps, DispatchProps, unknown, State>(
  mapStateToProps,
  mapDispatchToProps
)(App);
