import React, { FC, ReactElement } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import gradient from '../../images/cellGradientBackground@2x.png';
import Restaurant from '../../types/Restaurant';
import { Action, showDetails } from '../../redux/actions';

/**
 * Wrapped calls to action creators which automatically dispatch the action.
 */
interface DispatchProps {
  /** Dispatch wrapper for showDetails. */
  readonly dispatchShowDetails: (restaurant: Restaurant) => Action;
}

/**
 * Props passed to component from parent.
 */
interface OwnProps {
  readonly restaurant: Restaurant;
}

/**
 * Combined prop type.
 */
type Props = DispatchProps & OwnProps;

/**
 * A single restaurant in the list view.
 *
 * @constructor
 */
const RestaurantItem: FC<Props> = ({
  dispatchShowDetails,
  restaurant,
}: Props): ReactElement => (
  <div className='restaurant-item'>
    <button onClick={() => dispatchShowDetails(restaurant)} type='button'>
      <img alt={`${restaurant.name}`} src={restaurant.backgroundImageURL} />
      <img alt='gradient' className='gradient' src={gradient} />
      <div className='restaurant-item-name'>
        <h2>{restaurant.name}</h2>
        <h3>{restaurant.category}</h3>
      </div>
    </button>
  </div>
);

/**
 * Connect {@code DispatchProps} to Redux.
 */
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps =>
  bindActionCreators(
    {
      dispatchShowDetails: showDetails,
    },
    dispatch
  );

/*
 * Connect RestaurantItem to Redux.
 */
export default connect<unknown, DispatchProps, unknown, unknown>(
  null,
  mapDispatchToProps
)(RestaurantItem);
