import React, { FC, ReactElement } from 'react';

import Restaurant from '../../types/Restaurant';
import { getKey } from '../../util/getKey';

import RestaurantItem from './RestaurantItem';

/**
 * Props passed to component from parent.
 */
interface OwnProps {
  /** Restaurants to render in this row. */
  readonly restaurants: Restaurant[];
}

/**
 * A row of restaurants in the list view.
 *
 * @constructor
 */
const RestaurantRow: FC<OwnProps> = ({
  restaurants,
}: OwnProps): ReactElement => (
  <div className='flex-row'>
    {restaurants.map((restaurant) => (
      <RestaurantItem key={getKey(restaurant)} restaurant={restaurant} />
    ))}
  </div>
);

export default RestaurantRow;
