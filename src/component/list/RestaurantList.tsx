import React, { FC, ReactElement } from 'react';

import Restaurant from '../../types/Restaurant';
import { getKeys } from '../../util/getKey';
import partition from '../../util/partition';

import RestaurantRow from './RestaurantRow';

/**
 * Number of columns to display.
 */
const COLUMNS: number = 2;

/**
 * Props passed to component from parent.
 */
interface OwnProps {
  /** List of restaurants. */
  readonly restaurants: Restaurant[];
}

/**
 * List of rows containing {@code COLUMNS} number of {@code RestaurantItem}s.
 *
 * @constructor
 */
const RestaurantList: FC<OwnProps> = ({
  restaurants,
}: OwnProps): ReactElement => {
  const restaurantRows = partition(restaurants, COLUMNS);

  return (
    <div className='flex-column'>
      {restaurantRows.map((row) => (
        <RestaurantRow key={getKeys(row)} restaurants={row} />
      ))}
    </div>
  );
};

export default RestaurantList;
