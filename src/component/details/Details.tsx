import React, { FC, ReactElement } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Restaurant from '../../types/Restaurant';
import State from '../../types/State';
import { Action, hideDetails } from '../../redux/actions';

import ContactComponent from './Contact';
import Map from './Map';
import SlideOut from './SlideOut';

/**
 * Wrapped calls to action creators which automatically dispatch the action.
 */
interface DispatchProps {
  /** Dispatch wrapper for hideDetails. */
  readonly dispatchHideDetails: () => Action;
}

/**
 * State props which are wired in from Redux.
 */
interface StateProps {
  /** Restaurant to display, or {@code null} if the modal is closed. */
  readonly restaurant: Restaurant | null;
}

/**
 * Combined prop type.
 */
type Props = DispatchProps & StateProps;

/**
 * Slide out modal component which shows detailed information for a Restaurant.
 *
 * @constructor
 */
const Details: FC<Props> = ({
  dispatchHideDetails,
  restaurant,
}: Props): ReactElement => {
  if (!restaurant) {
    return <SlideOut isOpen={false} onClose={dispatchHideDetails} />;
  }

  return (
    <SlideOut isOpen onClose={dispatchHideDetails}>
      <Map
        address={restaurant.location.address}
        city={restaurant.location.city}
        name={restaurant.name}
        state={restaurant.location.state}
      />
      <div className='detail-name-wrapper'>
        <div className='detail-name'>
          <h2>{restaurant.name}</h2>
          <h3>{restaurant.category}</h3>
        </div>
      </div>
      <ContactComponent
        address={restaurant.location.address}
        city={restaurant.location.city}
        formattedPhone={restaurant.contact?.formattedPhone}
        postalCode={restaurant.location.postalCode}
        state={restaurant.location.state}
        twitter={restaurant.contact?.twitter}
      />
    </SlideOut>
  );
};

/**
 * Connect {@code DispatchProps} to Redux.
 */
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps =>
  bindActionCreators(
    {
      dispatchHideDetails: hideDetails,
    },
    dispatch
  );

/**
 * Connect {@code StateProps} to Redux.
 */
const mapStateToProps = (state: State): StateProps => ({
  restaurant: state.details,
});

/*
 * Connect Details component to Redux.
 */
export default connect<StateProps, DispatchProps, unknown, State>(
  mapStateToProps,
  mapDispatchToProps
)(Details);
