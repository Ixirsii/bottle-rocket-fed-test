import React, { FC, ReactNode } from 'react';

import close from '../../images/ic_close@2x.png';

/**
 * Props passed to component from parent.
 */
interface Props {
  /** Is the modal open? */
  readonly isOpen: boolean;
  /** Handler called when modal is closed. */
  readonly onClose: () => void;

  /** Content to display in the modal. */
  readonly children?: ReactNode;
}

/**
 * Slide out modal component.
 *
 * @constructor
 */
const SlideOut: FC<Props> = ({ children, isOpen, onClose }: Props) => {
  return (
    <div
      className='details'
      style={{
        boxShadow: isOpen ? '-5px 0 5px #444444' : 'none',
        transform: isOpen ? 'translateX(0%)' : 'translateX(100%)',
      }}
    >
      <div className='header'>
        <button className='close-details' onClick={onClose} type='button'>
          <img alt='close' src={close} />
        </button>
      </div>
      {children}
    </div>
  );
};

export default SlideOut;
