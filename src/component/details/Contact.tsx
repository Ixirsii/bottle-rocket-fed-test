import React, { FC, ReactElement } from 'react';

/**
 * Props passed to component from parent.
 */
interface Props {
  /** Location street address. */
  readonly address: string;
  /** Location city. */
  readonly city: string;
  /** Location post/zip code. */
  readonly postalCode: number;
  /** Location state. */
  readonly state: string;

  /** Formatted phone number (IE. (555) 555-5555). */
  readonly formattedPhone?: string;
  /** Twitter username. */
  readonly twitter?: string;
}

/**
 * Restaurant contact information for Details modal.
 *
 * @constructor
 */
const ContactComponent: FC<Props> = ({
  address,
  city,
  formattedPhone,
  postalCode,
  state,
  twitter,
}: Props): ReactElement => {
  const twitterLink: ReactElement | null = twitter ? (
    <p>
      <a
        href={`https://twitter.com/${twitter}`}
        rel='noopener noreferrer'
        target='_blank'
      >
        @{twitter}
      </a>
    </p>
  ) : null;

  return (
    <div className='contact'>
      <p>
        {address}
        <br />
        {`${city}, ${state} ${postalCode}`}
      </p>
      <p>{formattedPhone && formattedPhone}</p>
      {twitterLink}
    </div>
  );
};

export default ContactComponent;
