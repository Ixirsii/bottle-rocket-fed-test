import React, { FC, ReactElement } from 'react';

/** Google Maps API key. */
const API_KEY: string = 'AIzaSyDpnttwEk1oSreUPmdUXylvbP1yKu5WTiQ';
/** Google Maps endpoint. */
const ENDPOINT: string = 'https://www.google.com/maps/embed/v1';
/** Search mode for Google Maps API. */
const MODE: string = 'search';

/**
 * Props passed to component from parent.
 */
interface OwnProps {
  /** Location street address. */
  readonly address: string;
  /** Location city. */
  readonly city: string;
  /** Location name. */
  readonly name: string;
  /** Location state. */
  readonly state: string;
}

/**
 * Wrapper component for a Google Map iframe.
 *
 * @constructor
 */
const Map: FC<OwnProps> = ({
  address,
  city,
  name,
  state,
}: OwnProps): ReactElement => {
  const webAddress: string = escape(address);
  const webCity: string = escape(city);
  const webName: string = escape(name);
  const webState: string = escape(state);
  const search: string = `${webName},${webAddress},${webCity},${webState}`;

  return (
    <iframe
      src={`${ENDPOINT}/${MODE}?q=${search}&key=${API_KEY}`}
      title='map'
    />
  );
};

export default Map;
