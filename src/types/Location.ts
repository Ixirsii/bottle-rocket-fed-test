/**
 * Physical address.
 */
export default interface Location {
  /** Street address. */
  readonly address: string;
  /** Country code. */
  readonly cc: string;
  /** City. */
  readonly city: string;
  /** Country name. */
  readonly country: string;
  /** Array where element 0 is address line 1, element 1 is address line 2, etc. */
  readonly formattedAddress: string[];
  /** Latitude. */
  readonly lat: number;
  /** Longitude */
  readonly lng: number;
  /** Postal code. */
  readonly postalCode: number;
  /** State. */
  readonly state: string;

  /** Cross street. */
  readonly crossStreet?: string;
}
