import Contact from './Contact';
import Location from './Location';

/**
 * Restaurant.
 */
export default interface Restaurant {
  /** URL to the image to display for this restaurant. */
  readonly backgroundImageURL: string;
  /** What type of food the restaurant sells. */
  readonly category: string;
  /** Physical location. */
  readonly location: Location;
  /** Restaurant name. */
  readonly name: string;

  /** Contact details (phone number, social media accounts, etc). */
  readonly contact?: Contact;
}
