/**
 * Contact details.
 */
export default interface Contact {
  /** Formatted phone number (IE. (555) 555-5555). */
  readonly formattedPhone: string;
  /** Unformatted phone number (IE. 5555555555). */
  readonly phone: string;

  /** Facebook page ID. */
  readonly facebook?: string;
  /** Facebook page name. */
  readonly facebookName?: string;
  /** Facebook username. */
  readonly facebookUsername?: string;
  /** Twitter username. */
  readonly twitter?: string;
}
