import combinedReducer from '../redux/reducer';

type State = ReturnType<typeof combinedReducer>;

export default State;
