/**
 * Partition a list into sublists.
 *
 * @param input List to partition.
 * @param size Size of each partition.
 */
export default function partition<T>(input: T[], size: number): T[][] {
  const partitions: T[][] = [];

  for (let i = 0; i < input.length; i += size) {
    partitions.push(input.slice(i, i + size));
  }

  return partitions;
}
