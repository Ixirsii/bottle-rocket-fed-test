import Restaurant from '../types/Restaurant';

/**
 * Get a unique key from a restaurant for list rendering.
 *
 * @param restaurant Restaurant used to calculate key.
 */
export const getKey = (restaurant: Restaurant): string => {
  return `${restaurant.location.lat}-${restaurant.location.lng}`;
};

/**
 * Get a unique key for a list of restaurants for list rendering.
 *
 * @param restaurants Restaurants used to calculate key.
 */
export const getKeys = (restaurants: Restaurant[]): string => {
  return restaurants.reduce(
    (previousValue, currentValue) => `${previousValue},${getKey(currentValue)}`,
    ''
  );
};
