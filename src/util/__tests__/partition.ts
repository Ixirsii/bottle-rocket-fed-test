import partition from '../partition';

describe('Array partition utility', () => {
  it('partitions an array', () => {
    const numbers: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const size: number = 3;
    const expected: number[][] = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]];

    const actual = partition(numbers, size);

    expect(actual).toEqual(expected);
  });

  it('partitions an empty array', () => {
    const numbers: number[] = [];
    const size: number = 3;
    const expected: number[][] = [];

    const actual = partition(numbers, size);

    expect(actual).toEqual(expected);
  });
});
