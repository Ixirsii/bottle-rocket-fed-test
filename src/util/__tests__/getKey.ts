import { getKey, getKeys } from '../getKey';
import Restaurant from '../../types/Restaurant';

describe('Get key utility', () => {
  const lat1 = 1;
  const lat2 = 2;
  const lng1 = 3;
  const lng2 = 4;

  const restaurant1: Restaurant = {
    backgroundImageURL: 'https://my.web.site/image.png',
    category: 'Category',
    location: {
      address: '123 Street St.',
      cc: 'US',
      city: 'City',
      country: 'United States',
      formattedAddress: [],
      lat: lat1,
      lng: lng1,
      postalCode: 75022,
      state: 'TX',
    },
    name: 'Restaurant',
  };
  const restaurant2: Restaurant = {
    backgroundImageURL: 'https://my.web.site/image.png',
    category: 'Category',
    location: {
      address: '123 Street St.',
      cc: 'US',
      city: 'City',
      country: 'United States',
      formattedAddress: [],
      lat: lat2,
      lng: lng2,
      postalCode: 75022,
      state: 'TX',
    },
    name: 'Restaurant',
  };

  it('calculates a unique key from a Restaurant', () => {
    const expected = `${lat1}-${lng1}`;

    const actual = getKey(restaurant1);

    expect(actual).toBe(expected);
  });

  it('calculates a unique key from a list of Restaurants', () => {
    const expected = `,${lat1}-${lng1},${lat2}-${lng2}`;

    const actual = getKeys([restaurant1, restaurant2]);

    expect(actual).toBe(expected);
  });
});
