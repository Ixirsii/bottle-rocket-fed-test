import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reduxSaga from 'redux-saga';

import App from './component/App';
import './css/index.css';
import reportWebVitals from './util/reportWebVitals';
import combinedReducer from './redux/reducer';
import saga from './redux/saga';

// Redux Saga middleware.
const sagaMiddleware = reduxSaga();
// Create the Redux store.
const store = createStore(
  combinedReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

// Run the Saga middleware.
sagaMiddleware.run(saga);

// Render the application.
ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
